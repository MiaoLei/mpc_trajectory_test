#include <model_predictive_navigation/control_law.h>
#include <model_predictive_navigation/EgoGoal.h>

#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32MultiArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/GridCells.h>
#include <tf/transform_datatypes.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h>
#include <navfn/navfn_ros.h>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>

#include <math.h>
#include <string>
#include <vector>
//#include </home/alex/cppad/cppad/ipopt/solve.hpp>
//#include </home/alex/cppad/cppad/cppad.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <cppad/cppad.hpp>
#include <cppad/ipopt/solve.hpp>
#include <nlopt.hpp>
using ::model_predictive_navigation::ControlLaw;
using ::model_predictive_navigation::ControlLawSettings;
using ::model_predictive_navigation::EgoPolar;
using ::model_predictive_navigation::EgoGoal;
using namespace CA;



#define RATE_FACTOR 0.5
#define DEFAULT_LOOP_RATE 25

#define RESULT_BEGIN 1
#define RESULT_SUCCESS 2
#define RESULT_CANCEL 3

// Trajectory model parameters
#define K_1 1.2           // 2
#define K_2 3             // 8
#define BETA 0.4          // 0.5
#define LAMBDA 2          // 3
#define R_THRESH 0.15
#define V_MAX 0.3         // 0.3
#define V_MIN 0.00

#define R_SPEED_LIMIT 0.5
#define V_SPEED_LIMIT 0.3

// Parameters to determine success
#define GOAL_DIST_UPDATE_THRESH   0.15   // in meters
#define GOAL_ANGLE_UPDATE_THRESH  0.1   // in radians

#define GOAL_DIST_ID_THRESH   0.1       // in meters
#define GOAL_ANGLE_ID_THRESH  0.3       // in radians

static const double PI= 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348;
static const double TWO_PI= 6.2831853071795864769252867665590057683943387987502116419498891846156328125724179972560696;
static const double minusPI= -3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348;



//using namespace CA;
ca_common::Trajectory path;
CA::Vector3D curr_position;
CA::Vector3D curr_velocity;
CA::State curr_state;
ros::Time lastPlan;
std::vector<geometry_msgs::PoseStamped> current_goals;
geometry_msgs::PointStamped global_goal_pose;



void pathCallback(const ca_common::Trajectory::ConstPtr& msg);

void pathCallback(const ca_common::Trajectory& msg)
    {
        geometry_msgs::PoseStamped state;

        path.fromMsg(*msg);
        state.pose.position.x=msg.Trajectory_.trajectory.vector[0];
        state.pose.position.y=msg.Trajectory_.trajectory.vector[1];
        state.pose.position.z=msg.Trajectory_.trajectory.vector[2];
        lastPlan = msg->header.stamp;
        path.fromMsg(*msg);
        controllerState.closestIdx = 0.0;
    }
void receive_goals(const geometry_msgs::PoseStamped::ConstPtr& goal)
{
  boost::mutex::scoped_lock lock(goals_mutex_);

  bool bIsSameGoal = same_global_goal(*goal);

  current_goals.clear();
  current_goals.push_back(*goal);

  if (bHasOdom && bHasCostMap)
  {
    global_goal_pose = *goal;
  }
  else
  {
    ROS_DEBUG("[MPEPC] goal recieved but no odom or cost map");
    return;
  }

  if (bFollowingTrajectory && !bIsSameGoal)
  {
    ROS_DEBUG("cancelling current goal");
    cancel_auton_nav(RESULT_CANCEL);
  }

  bHasGoal = true;

  if (!bFollowingTrajectory && bHasGoal)
  {
    ROS_INFO("Begin path planning and execution");
    rviz_goal_request_pub.publish(global_goal_pose);
    bFollowingTrajectory = true;
    result.data = RESULT_BEGIN;
    goal_request_pub.publish(global_goal_pose);
    nav_result_pub.publish(result);
  }
}


void odometryCallback(const nav_msgs::Odometry::ConstPtr & msg);

void odometryCallback(const nav_msgs::Odometry::ConstPtr & msg)
    {
        curr_state =msgc(*msg);

    }





nav_msgs::Odometry current_pose;
bool bFollowingTrajectory, bHasGoal, bHasIntGoal, prevButton, bHasCostMap, bHasOdom;
ros::Publisher cmd_pub, current_goal_pub, nav_result_pub, goal_request_pub, rviz_goal_request_pub, min_Obstacle_pub;
ros::Subscriber goal_sub, odom_sub, joy_sub, cost_map_sub, int_goal_pose_sub;




int main(int argc, char** argv)
{
  // Initialize ROS
  ros::init(argc, argv, "mpc_trajectory");
  ros::NodeHandle n;
  nh = &n;

  // Setup control law for trajectory generation
  ControlLawSettings settings;
  settings.m_K1 = K_1;
  settings.m_K2 = K_2;
  settings.m_BETA = BETA;
  settings.m_LAMBDA = LAMBDA;
  settings.m_R_THRESH = R_THRESH;
  settings.m_V_MAX = V_MAX;
  settings.m_V_MIN = V_MIN;
  ControlLaw claw(settings);
  cl = &claw;

  current_pose.pose.pose.orientation.x = 0;
  current_pose.pose.pose.orientation.y = 0;
  current_pose.pose.pose.orientation.z = 0;
  current_pose.pose.pose.orientation.w = 1;

  bFollowingTrajectory = false;
  bHasGoal = false;
  bHasIntGoal = false;
  prevButton = false;
  bHasOdom = false;
  bHasCostMap = true;


  ros::Subscriber path_sub = n.subscribe<ca_common::Trajectory>("path", 10, pathCallback);
  ros::Subscriber odometry_sub = n.subscribe<nav_msgs::Odometry>("odometry", 10, odometryCallback, hints);

  goal_sub = nh->subscribe<geometry_msgs::PoseStamped>("/mpepc_goal_request", 1, receive_goals);
  cmd_pub = nh->advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1);

  nav_result_pub = nh->advertise<std_msgs::Int32>("nav_result", 1);
  goal_request_pub = nh->advertise<geometry_msgs::PoseStamped>("goal_request", 1);
  rviz_goal_request_pub = nh->advertise<geometry_msgs::PoseStamped>("rviz_goal_request", 1);

  odom_sub = nh->subscribe<nav_msgs::Odometry>("/odom", 1, odom_cb);
  int_goal_pose_sub = nh->subscribe<model_predictive_navigation::EgoGoal>("int_goal_pose", 1, int_goal_cb);
  cost_map_sub = nh->subscribe<nav_msgs::GridCells>("/costmap_translator/obstacles", 100, nav_cb);

  // ros::Rate loop_rate(20.0);
  double loop_rate = DEFAULT_LOOP_RATE;
  double global_loop_rate;
  if (ros::param::has("/global_loop_rate"))
  {
    nh->getParam("/global_loop_rate", global_loop_rate);
    loop_rate= RATE_FACTOR*global_loop_rate;
    ROS_DEBUG("Smooth_Traj: Loop rate updated using global_loop_rate to : %f", loop_rate);
  }
  ros::Rate rate_obj(loop_rate);

  EgoPolar global_goal_coords;
  geometry_msgs::Twist cmd_vel;

  // Control Loop and code
  while (ros::ok())
  {
    if (bFollowingTrajectory && bHasIntGoal)  // autonomous trajectory generation and following mode
    {
      global_goal_coords = cl->convert_to_egopolar(current_pose, global_goal_pose.pose);
      if (global_goal_coords.r <= GOAL_DIST_UPDATE_THRESH)//reach goal distance check.
      {
        double angle_error = tf::getYaw(current_pose.pose.pose.orientation) - tf::getYaw(global_goal_pose.pose.orientation);
        angle_error = cl->wrap_pos_neg_pi(angle_error);  //rad to angle
        if (fabs(angle_error) > GOAL_ANGLE_UPDATE_THRESH)  //reach goal angle check.
        {
          cmd_vel.linear.x = 0;
          if (angle_error > 0)
            cmd_vel.angular.z = -4 * settings.m_V_MIN;
          else
            cmd_vel.angular.z = 4 * settings.m_V_MIN;
        }
        else
        {
          ROS_DEBUG("[MPEPC] Completed normal trajectory following");
          cancel_auton_nav(RESULT_SUCCESS);
          cmd_vel.linear.x = 0;
          cmd_vel.angular.z = 0;
        }
      }
      else
      {
        cmd_vel = cl->get_velocity_command(current_pose, inter_goal_pose.pose, inter_goal_k1, inter_goal_k2, inter_goal_vMax);
      }
    }
    cmd_pub.publish(cmd_vel);

    ros::spinOnce();
    rate_obj.sleep();
  }
}


