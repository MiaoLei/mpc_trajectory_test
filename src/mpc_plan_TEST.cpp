#include "ros/ros.h"
#include <geometry_msgs/Vector3.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float64.h>
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <visualization_msgs/Marker.h>
#include <mk_model/mk_common.h>
//#include <trajectory_control/trajectory_control_lib.h>
//#include <trajectory_control/Command.h>
#include <riverine_watchdog/watchdog.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
#include "Eigen-3.3/Eigen/Core"
#include "Eigen-3.3/Eigen/QR"
#include "MPC.h"

using namespace CA;
CA::Trajectory path;
CA::Vector3D curr_position;
CA::Vector3D curr_velocity;
CA::State curr_state;
ros::Time lastPlan;

//model parameters
vector<double> ptsx,ptsy;
const double px,py,psi,v_raw,v,angle,throttle;


void pathCallback(const ca_common::Trajectory::ConstPtr& msg);
void odometryCallback(const nav_msgs::Odometry::ConstPtr & msg);
void pathCallback(const ca_common::Trajectory::ConstPtr& msg)
    {
    lastPlan = msg->header.stamp;
    ptsx=msg.element_type.trajectory[0];
    ptsy=msg.element_type.trajectory[1];



    }

void odometryCallback(const nav_msgs::Odometry::ConstPtr & msg)
    {
px=msg->pose.pose.orientation.x;
py=msg->pose.pose.orientation.y;
v=msg->twist.twist.linear[0];
angle=msg->pose.pose.orientation_rad[2];




    }





int main(int argc, char **argv)
{
  ros::init(argc, argv, "mpc_trajectory");
  ros::NodeHandle n("mpc_trajectory");
  ros::Subscriber path_sub = n.subscribe<ca_common::Trajectory>("path", 10, pathCallback);
  ros::Subscriber odometry_sub = n.subscribe<nav_msgs::Odometry>("odometry", 10, odometryCallback, hints);








}
