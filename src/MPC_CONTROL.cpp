#include "ros/ros.h"
#include <geometry_msgs/Vector3.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float64.h>
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <visualization_msgs/Marker.h>
#include <mk_model/mk_common.h>
//#include <thread>
#include <math.h>
//#include <trajectory_control/trajectory_control_lib.h>
//#include <trajectory_control/Command.h>
#include <riverine_watchdog/watchdog.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
#include "iostream"


#include "Eigen/Core"
#include "Eigen/QR"
#include "Eigen/Householder"
#include "Eigen/SparseQR"


#include <vector>

#include "MPC.h"
#include "MPC.cpp"





using namespace std;
using namespace Eigen;
using namespace CA;



CA::Trajectory path;
CA::Vector3D curr_position;
CA::Vector3D curr_velocity;
CA::State curr_state;
ros::Time lastPlan;
MPC mpc;




//model parameters
vector<double> ptsx,ptsy;
 double px,py,psi,v_raw,v,angle,throttle;
int wptsN;
const double cospsi = cos(-psi);
const double sinpsi = sin(-psi);
Eigen::VectorXd coeffs;
const double Lf = LF;
const double dt =  1/5;

//
void pathCallback(const ca_common::Trajectory::ConstPtr& msg);
void odometryCallback(const nav_msgs::Odometry::ConstPtr & msg);


void pathCallback(const ca_common::Trajectory::ConstPtr& msg)
    {
    path.fromMsg(*msg);
    lastPlan = msg->header.stamp;
    // ptsx=msg->trajectory[0];
    wptsN=msg->trajectory.size();
    ptsx.resize(wptsN);
    ptsy.resize(wptsN);
    for( int i=0;i<wptsN;i++)
      {
        ptsx[i]=msg->trajectory[i].position.x;
        ptsy[i]=msg->trajectory[i].position.y;
      }
    }

void odometryCallback(const nav_msgs::Odometry::ConstPtr & msg)
    {
    curr_state =msgc(*msg);
    px=msg->pose.pose.orientation.x;
    py=msg->pose.pose.orientation.y;
    v=msg->twist.twist.linear.x;
    angle=curr_state.pose.orientation_rad[2];
    throttle=0.1;
    // Convert to the vehicle coordinate system
    VectorXd x_veh(wptsN);
    VectorXd y_veh(wptsN);
    for(int i = 0; i < wptsN; i++)
      {
        const double dx = ptsx[i] - px;
        const double dy = ptsy[i] - py;
        x_veh[i] = dx * cospsi - dy * sinpsi;
        y_veh[i] = dy * cospsi + dx * sinpsi;
      }


    }

double polyeval(Eigen::VectorXd coeffs, double x)
{
  double result = 0.0;
  for (int i = 0; i < coeffs.size(); i++) {
    result += coeffs[i] * pow(x, i);
  }
  return result;
}


Eigen::VectorXd polyfit(Eigen::VectorXd xvals, Eigen::VectorXd yvals, int order)
{
  assert(xvals.size() == yvals.size());
  assert(order >= 1 && order <= xvals.size() - 1);
  MatrixXd A(xvals.size(), order + 1);
  MatrixXd Q,result;

  for (int i = 0; i < xvals.size(); i++)
    {
      A(i, 0) = 1.0;
    }
  for (int j = 0; j < xvals.size(); j++)
    {
      for (int i = 0; i < order; i++)
      {
        A(j, i + 1) = A(j, i) * xvals(j);
      }
    };



    //auto Q = A.householderQr();
    //auto result = Q.solve(yvals);




    result = A.householderQr().solve(yvals);   //compute 'result':  A*result=yvals



    return result;


}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "mpc_trajectory");
  ros::NodeHandle n("mpc_trajectory");
  ros::TransportHints hints = ros::TransportHints().udp().tcpNoDelay();
  ros::Subscriber path_sub = n.subscribe<ca_common::Trajectory>("path", 10, pathCallback);
  ros::Subscriber odometry_sub = n.subscribe<nav_msgs::Odometry>("odometry", 10, odometryCallback, hints);



  ros::Publisher twist_command_pub = n.advertise<geometry_msgs::TwistStamped>("/command/twist", 100); //output translation
  ros::Rate loop_rate(5);
//MPC MODEL PARAMETERS
  VectorXd x_veh(N);
  VectorXd y_veh(N);
   coeffs = polyfit(x_veh, y_veh, 3);
  const double cte = coeffs[0];
  const double epsi = -atan(coeffs[1]);
  const double px_act = v * dt;
  const double py_act = 0;
  const double psi_act = - v * angle * dt / Lf;
  const double v_act = v + throttle * dt;
  const double cte_act = cte + v * sin(epsi) * dt;
  const double epsi_act = epsi + psi_act;
  VectorXd state(6);
  state << px_act, py_act, psi_act, v_act, cte_act, epsi_act;
  vector<double> mpc_results = mpc.Solve(state, coeffs);
  double angle_value = mpc_results[0];
  double throttle_value = mpc_results[1];


  geometry_msgs::TwistStamped twist;
  twist.header.stamp = ros::Time::now();
  twist.header.seq++;
  twist.twist.linear.x = v+throttle_value*dt;
  twist.twist.angular.x=twist.twist.angular.y=0.0;
  twist.twist.angular.z=angle+angle_value*dt;
  twist_command_pub.publish(twist);






    /*if(path.size() < 1 || (ros::Time::now() - lastPlan).toSec() > 1.0)
           {
               ROS_WARN_STREAM("Trajectory_control: Path does not contain any waypoints, default to position hold hover");
               ca_common::Trajectory hoverMsg;
               hoverMsg.header.stamp = ros::Time::now();
               hoverMsg.header.frame_id = "/world";

               ca_common::TrajectoryPoint hoverTrajPoint;
               hoverTrajPoint.position.x = curr_state.pose.position_m[0];
               hoverTrajPoint.position.y = curr_state.pose.position_m[1];
               hoverTrajPoint.position.z = curr_state.pose.position_m[2];
               hoverTrajPoint.heading = curr_state.pose.orientation_rad[2];

               hoverMsg.trajectory.push_back(hoverTrajPoint);

               path.fromMsg(hoverMsg);
               controllerState.closestIdx = 0.0;

           }
    */
}
